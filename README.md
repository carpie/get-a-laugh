Sample application for my Managing UI Complexity With Statecharts YouTube video
series. This is the non-state chart version.

Accompanying video: https://youtu.be/zfkqNQlijYM

## Quickstart

These instructions assume you have node.js installed. If not, I suggest
installing via [nvm](https://github.com/nvm-sh/nvm#installing-and-updating).

```
git clone https://gitlab.com/carpie/get-a-laugh
cd get-a-laugh
npm ci
npm start
```

A browser tab should open automatically with the running application.

## Attribution

The sample application pulls
[CCA-NC](https://creativecommons.org/licenses/by-nc/2.5/) licensed
[xkcd](xkcd.com) data via xkcd.now.sh and displays it unmodified.
