import React, { useState } from 'react';
import { LoadingSpinner } from './LoadingSpinner';
import { fetchLaughs } from './fetchLaughs';
import './styles.css';

export default function App() {
  const [fetchCount, setFetchCount] = useState(0);
  const [comicImg, setComicImg] = useState('');
  const [hasLoadedComic, setHasLoadedComic] = useState(false);
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  function getALaugh() {
    console.log('laugh requested');
    if (isLoading) {
      return;
    }
    setFetchCount(count => count += 1);
    setIsLoading(true);
    fetchLaughs()
      .then(data => {
        console.log('data', data);
        setComicImg(data.img);
        setHasLoadedComic(true);
        setError('');
        setIsLoading(false);
      })
      .catch(err => {
        console.log('err', err); // This will never happen
        setError(err.message);
        setIsLoading(false);
      });
  }

  function renderError() {
    if (isLoading) {
      return <LoadingSpinner />
    }
    return (
      <>
        <h1>Request to laugh denied!</h1>
        <h2>{error}</h2>
      </>
    );
  }

  function renderComic() {
    if (isLoading) {
      return <LoadingSpinner />
    }
    if (!hasLoadedComic) {
      return <h2>Click the button to get a laugh...</h2>;
    }
    return <img src={comicImg} alt="A funny comic" />;
  }

  return (
    <div className="App">
      <div className="App__actionArea">
        <button onClick={getALaugh}>Get A Laugh</button>
        <div>
          Laughs requested: {fetchCount}
        </div>
      </div>
      <div className="App__comic">
        {error ? renderError() : renderComic()}
      </div>
    </div>
  );
}
